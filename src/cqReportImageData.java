import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.naming.NamingException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.NotBoundException;

public class cqReportImageData {
    private FileWriter fw;

    public static void main(String[] args) throws ClassCastException, IOException, NotBoundException, LoginException, NoSuchWorkspaceException, RepositoryException, NamingException {
        System.out.println("Welcome to the scripps cq reports interface.");
        System.out.println("Please wait while the report is processing.....");
        cqReportImageData me = new cqReportImageData();
        me.run();
        System.out.println("Finished processing the report");
    }

    public void run() throws RepositoryException, IOException {
        File newTextFile = new File("/tmp/cqReport.csv");
        fw = new FileWriter(newTextFile);

        Repository repository = JcrUtils.getRepository("http://author.hgtv-dev.sni.hgtv.com/crx/server");
        SimpleCredentials creds = new SimpleCredentials("admin", "admin".toCharArray());
        Session session = repository.login(creds);
        Node root = session.getRootNode();

        fw.append("IMAGE REPORT" + "\n");
        fw.append("cq:path,cq:lastReplicationAction" + "\n");
        drillChildren(root.getNode("content/dam/images/hgtv/fullset/2002"));
        fw.close();
    }

    private void drillChildren(Node node) throws RepositoryException, IOException {
        NodeIterator nia = node.getNodes();
        while (nia.hasNext()) {
            Node na = nia.nextNode();
            if (na.getName().equals("jcr:content") && !na.hasNodes()) {
                getProperities(na);
            } else {
                drillChildren(na);
            }
        }
    }

    private void getProperities(Node node) throws RepositoryException, IOException {
        String l_Field = "";
        String v_lastReplicationAction = "";

        PropertyIterator pi = node.getProperties();
        while (pi.hasNext()) {
            Property p = pi.nextProperty();
            l_Field = p.getName();
            if (l_Field.equals("cq:lastReplicationAction")) {
                if (p.getDefinition().isMultiple()) {
                    Value[] values = p.getValues();
                    for (int i = 0; i < values.length; i++) {
                        v_lastReplicationAction = values[i].getString();
                    }//End of values for loop
                } else {
                    v_lastReplicationAction = p.getValue().getString();
                }//End if isMultiple
            }
            //System.out.println(node.getPath()+","+v_lastReplicationAction+"\n");

        }
        fw.append(node.getPath() + "," + v_lastReplicationAction + "\n");
    }


}
