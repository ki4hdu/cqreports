import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.naming.NamingException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class BulkImageUpdateTest {
    public static String svalues = "";

    public static void main(String[] args) throws ClassCastException, MalformedURLException, RemoteException, NotBoundException, LoginException, NoSuchWorkspaceException, RepositoryException, NamingException {
        System.out.println("Welcome to the scripps cq updates interface.");
        System.out.println("Please wait while the updates are processing.....");
        Repository repository = JcrUtils.getRepository("http://cctvauthdev.scrippsnetworks.com/crx/server"); //http://localhost:4502/crx/server
                  /* Update xxxxx with your username and password before you run this */
        SimpleCredentials creds = new SimpleCredentials("xxxxx", "xxxxx".toCharArray());
        Session session = null;
        session = repository.login(creds, "crx.default");
        Node root = session.getRootNode();

        drillChildrenList(root.getNode("content/dam/images/cook/"));
    }


    public static void drillChildrenList(Node node) throws RepositoryException {

        List<Node> n_list = new ArrayList<Node>();

        for (NodeIterator nia = node.getNodes(); nia.hasNext(); ) {
            n_list.add(nia.nextNode());
        }
        drillChildren(n_list, 0);

        //now iterate over list and modify
        //  for(Node na in n_list) {
        //       setProperity(na);
        // }
    }

    public static void drillChildren(List<Node> nodes, int index) throws RepositoryException {
                  /* we're already at the right level */
        Node na = nodes.get(index);
        if (na.getName().equals("jcr:content") && na.hasNodes()) {
            System.out.println(na.getPath());
            //setProperity(na);
                        /* just increment the index, and leave the item in the list */
            index++;
            //drillChildren(nodes, index);
        } else {
                  /* does this item have more nodes? */
            if (na.hasNodes()) {
                for (NodeIterator nia = na.getNodes(); nia.hasNext(); ) {
                    nodes.add(nia.nextNode());
                }
                //nodes.remove(na);
                index = 0;
                //drillChildren(nodes, index);
            } else {
                index++;
            }
                        /* get rid of the "parent" node, because it's not a jcr:content node and/or has nothing under it */
            nodes.remove(na);
        }
                  /* recursion terminator - recurse if we are not at the end */
        if (index <= (nodes.size() - 1))
            drillChildren(nodes, index);
    }

    public static void setProperity(Node node) throws RepositoryException {
        node.setProperty("cq:lastReplicationAction", "Activate");  // <-- Set the property here
        node.save();
    }

}
