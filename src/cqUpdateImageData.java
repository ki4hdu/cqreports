import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.naming.NamingException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class cqUpdateImageData {
    public static String svalues = "";

    public static void main(String[] args) throws ClassCastException, MalformedURLException, RemoteException, NotBoundException, LoginException, NoSuchWorkspaceException, RepositoryException, NamingException {
        System.out.println("Welcome to the scripps cq updates interface.");
        System.out.println("Please wait while the updates are processing.....");
        Repository repository = JcrUtils.getRepository("http://cctvauthdev.scrippsnetworks.com/crx/server"); //http://localhost:4502/crx/server
        SimpleCredentials creds = new SimpleCredentials("xxxxx", "xxxxx".toCharArray()); // <--- Update xxxxx with your username and password before you run this
        Session session = null;
        session = repository.login(creds, "crx.default");
        Node root = session.getRootNode();

        // Retrieve content from root node
        drillChildren(root.getNode("content/dam/images/cook/fullset/2010/10/1/0"));  // <-- Set the path of the images here, for testing i was just using few images

        System.out.println("Finished processing the updates");
    }

    public static void drillChildren(Node node) throws RepositoryException {

        for (NodeIterator nia = node.getNodes(); nia.hasNext(); ) {
            Node na = nia.nextNode();

            if (na.getName().equals("jcr:content") && na.hasNodes()) {
                System.out.println(na.getPath());
                setProperity(na);
            } else {
                drillChildren(na);
            }

        }
    }

    public static void setProperity(Node node) throws RepositoryException {

        node.setProperty("cq:lastReplicationAction", "Activate");  // <-- Set the property here
        node.save();

    }


}

