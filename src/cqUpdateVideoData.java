import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.naming.NamingException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class cqUpdateVideoData {
    public static String svalues = "";

    public static void main(String[] args) throws ClassCastException, MalformedURLException, RemoteException, NotBoundException, LoginException, NoSuchWorkspaceException, RepositoryException, NamingException {
        System.out.println("Welcome to the scripps cq updates interface.");
        System.out.println("Please wait while the updates are processing.....");
        Repository repository = JcrUtils.getRepository("http://localhost:4502/crx/server");
        SimpleCredentials creds = new SimpleCredentials("admin", "admin".toCharArray());
        Session session = null;
        session = repository.login(creds, "crx.default");
        Node root = session.getRootNode();

        // Retrieve content from root node
        drillChildren(root.getNode("etc/sni-asset/videos/2010/10/11/0"));

        System.out.println("Finished processing the updates");
    }

    public static void drillChildren(Node node) throws RepositoryException {

        for (NodeIterator nia = node.getNodes(); nia.hasNext(); ) {
            Node na = nia.nextNode();

            if (na.getName().equals("jcr:content") && na.hasNodes()) {
                System.out.println(na.getPath());
                setProperity(na);
            } else {
                drillChildren(na);
            }

        }
    }

    public static void setProperity(Node node) throws RepositoryException {

        node.setProperty("cq:lastReplicationAction", "Activate");
        node.save();

    }


}

