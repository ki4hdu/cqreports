import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.naming.NamingException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;

public class cqReport {

    public static void main(String[] args) throws ClassCastException, MalformedURLException, RemoteException, NotBoundException, LoginException, NoSuchWorkspaceException, RepositoryException, NamingException {
        System.out.println("Welcome to the scripps cq reports interface.");
        //Connection
        //Repository repository = JcrUtils.getRepository("http://localhost:4502/crx/server");
        //Repository repository = JcrUtils.getRepository("http://auth.scrippsnetworks.com/crx/server");
        Repository repository = JcrUtils.getRepository("http://author.hgtv-dev.sni.hgtv.com/crx/server");
        //Workspace Login
        SimpleCredentials creds = new SimpleCredentials("admin", "admin".toCharArray());
        Session session = repository.login(creds, "crx.default");

        String user = session.getUserID();
        String name = repository.getDescriptor(Repository.REP_NAME_DESC);
        System.out.println("Logged in as " + user + " to a " + name + " repository.");

        //List name of the current workspace
        System.out.println("Workspace: " + session.getWorkspace().getName() + "\n");

        //Create a node that represents the root node
        Node root = session.getRootNode();
        System.out.println("Root Node: " + root.getPath());
        //System.out.println(root.getProperty("sling:target").getString());

        // Retrieve content from root node
        printChildren(root.getNode("content/cook/recipes/photo-galleries"));
    }

    public static void printChildren(Node node) throws RepositoryException {

        String svalues = "";
        svalues = svalues + "RECIPE PHOTO GALLERY REPORT" + "\n";
        svalues = svalues + "jcr:title,jcr:uuid,sni:sponsorships";
        // Loop through each child(i.e Each Recipe Parent Node)
        for (NodeIterator nia = node.getNodes(); nia.hasNext(); ) {
            Node na = nia.nextNode();
            // Loop through each child(i.e Each Recipe Node)
            for (NodeIterator ni = na.getNodes(); ni.hasNext(); ) {
                Node n = ni.nextNode();
                String firstvalue = n.getName();
                // Check only contents from jcr:content node
                if (firstvalue.equals("jcr:content")) {
                    System.out.println("Recipe Parent Node Name is " + n.getName());
                } else {
                    String lvalues = "";
                    System.out.println("Recipe Node name is " + n.getName());
                    svalues = svalues + getSelectedFields(n, lvalues) + "\n";

                } // End of If for checking jcr:content
            } // End of the for n loop
        }// End of the for na loop
        //Write the report into a csv or text file
        exportData(svalues);
    } //End of Printchildren method

    private static String getSelectedFields(Node node, String lvalues) throws RepositoryException {
        lvalues = "";
        String l_Field = "";
        String v_uuid = "";
        String v_title = "";
        String v_sponsorship = "";
        Boolean properityexists = true;
        //loop through underlying nodes
        for (NodeIterator ni = node.getNodes(); ni.hasNext(); ) {
            Node n = ni.nextNode();
            System.out.println("Node name is " + n.getName());
            //Look into jcr:content node only
            if (n.getName().equals("jcr:content")) {
                // Look for the properties(fields)
                for (PropertyIterator pi = n.getProperties(); pi.hasNext(); ) {
                    Property p = pi.nextProperty();
                    l_Field = p.getName();
                    System.out.println("Field name is " + p.getName());
                    switch (l_Field) {
                        case "jcr:uuid":
                            if (p.getDefinition().isMultiple()) {
                                Value[] values = p.getValues();
                                for (int i = 0; i < values.length; i++) {
                                    v_uuid = values[i].getString();
                                }//End of values for loop
                            } else {
                                v_uuid = p.getValue().getString();
                            }//End if isMultiple
                            break;
                        case "jcr:title":
                            if (p.getDefinition().isMultiple()) {
                                Value[] values = p.getValues();
                                for (int i = 0; i < values.length; i++) {
                                    v_title = values[i].getString();
                                }//End of values for loop
                            } else {
                                v_title = p.getValue().getString();
                            }//End if isMultiple
                            break;
                        case "sni:sponsorships":
                            if (p.getDefinition().isMultiple()) {
                                Value[] values = p.getValues();
                                for (int i = 0; i < values.length; i++) {
                                    v_sponsorship = values[i].getString();
                                }//End of values for loop
                            } else {
                                v_sponsorship = p.getValue().getString();
                            }//End if isMultiple
                            break;

                        default:
                            break;
                    }//End switch statement

                }//End for property iterator

                System.out.println("After properity iterator the title is " + v_title + " and uuid is " + v_uuid + " and sponsorship is" + v_sponsorship);
                lvalues = lvalues + v_title.replace(",", "") + "," + v_uuid + "," + v_sponsorship;

            }//End if jcr:content validation

        }//End node iterator
        return lvalues;
    }//End getSelectedFields

    private static String getProperities(Node node, String lvalues) throws RepositoryException {
        Hashtable<String, String> report = new Hashtable<String, String>();

        lvalues = "";
        for (NodeIterator ni = node.getNodes(); ni.hasNext(); ) {
            Node n = ni.nextNode();

            //System.out.println("Node name is "+ n.getName());
            if (n.getName().equals("jcr:content")) {
                for (PropertyIterator pi = n.getProperties(); pi.hasNext(); ) {
                    Property p = pi.nextProperty();
                    if (p.getDefinition().isMultiple()) {
                        Value[] values = p.getValues();
                        for (int i = 0; i < values.length; i++) {
                            //System.out.println("  - " + p.getName() + "\t " + values[i].getString());
                            lvalues = lvalues + n.getName() + "-->" + "\t" + p.getName() + "\t " + values[i].getString() + "\n";
                            report.put(p.getName(), values[i].getString());
                        } //End of for loop
                    } else {
                        //System.out.println("  - " + p.getName() + "\t " + p.getValue().getString());
                        lvalues = lvalues + n.getName() + "-->" + "\t" + p.getName() + "\t " + p.getValue().getString() + "\n";
                        report.put(p.getName(), p.getValue().getString());
                    }//End If statement

                }//End of For of property Statement
                // lvalues=lvalues+"\n";
            }//End of For of node
        }//End of If for jcr:content
        System.out.println("Printing hashtable contents");
        System.out.println("Size of hashtable in Java: " + report.size());
        Enumeration enumeration = report.elements();
        enumeration = report.keys();
        while (enumeration.hasMoreElements()) {
            String str = (String) enumeration.nextElement();
            System.out.println(str + ": " +
                    report.get(str));
        }
        //while (enumeration.hasMoreElements()) {
        //	System.out
        //	.println("hashtable values: " + report.keys()+" "+enumeration.nextElement());
        //	}
        return lvalues;
    }


    private static void exportData(String outtext) {


        try {
            String str = outtext;
            File newTextFile = new File("C:/files/Spool/Java/cq/Output/output.csv");

            FileWriter fw = new FileWriter(newTextFile);
            fw.append(str);
            fw.close();

        } catch (IOException iox) {
            //do stuff with exception
            iox.printStackTrace();
        }

    }
}
            
 
    
