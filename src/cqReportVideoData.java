import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.naming.NamingException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class cqReportVideoData {
    public static String svalues = "";

    public static void main(String[] args) throws ClassCastException, MalformedURLException, RemoteException, NotBoundException, LoginException, NoSuchWorkspaceException, RepositoryException, NamingException {
        System.out.println("Welcome to the scripps cq reports interface.");
        System.out.println("Please wait while the report is processing.....");
        Repository repository = JcrUtils.getRepository("http://auth.scrippsnetworks.com/crx/server");
        SimpleCredentials creds = new SimpleCredentials("scrippsreport", "scripps1".toCharArray());
        Session session = null;
        session = repository.login(creds, "crx.default");
        Node root = session.getRootNode();

        appendString(svalues);
        // Retrieve content from root node
        drillChildren(root.getNode("etc/sni-asset/videos"));
        exportData(svalues);
        System.out.println("Finished processing the report");
    }

    public static void drillChildren(Node node) throws RepositoryException {

        for (NodeIterator nia = node.getNodes(); nia.hasNext(); ) {
            Node na = nia.nextNode();

            if (na.getName().equals("jcr:content") && !na.hasNodes()) {
                getProperities(na);
            } else {
                drillChildren(na);
            }

        }
    }

    public static void getProperities(Node node) throws RepositoryException {
        String l_Field = "";
        String v_searchable = "";
        String nodepath = "";

        for (PropertyIterator pi = node.getProperties(); pi.hasNext(); ) {
            Property p = pi.nextProperty();

            l_Field = p.getName();
            switch (l_Field) {

                case "sni:searchable":
                    if (p.getDefinition().isMultiple()) {
                        Value[] values = p.getValues();
                        for (int i = 0; i < values.length; i++) {
                            v_searchable = values[i].getString();
                        }//End of values for loop
                    } else {
                        v_searchable = p.getValue().getString();
                    }//End if isMultiple
                    break;

                default:
                    break;
            }//End switch statement
            //System.out.println(node.getPath()+","+v_lastReplicationAction+"\n");

        }
        //nodepath=node.getPath().toString();
        //nodepath=nodepath.replace("jcr:content", null);
        appendString(node.getPath() + "," + v_searchable + "\n");


    }

    private static void appendString(String text) {
        if (text.isEmpty()) {
            svalues = "VIDEO SEARCHABLE FLAG REPORT" + "\n";
            svalues = svalues + "cq:path,sni:serachable" + "\n";
        } else {
            svalues = svalues + text;
        }
    }

    private static void exportData(String outtext) {


        try {
            String str = outtext;
            File newTextFile = new File("C:/files/Spool/Java/cq/Output/output.csv");

            FileWriter fw = new FileWriter(newTextFile);
            fw.append(str);
            fw.close();

        } catch (IOException iox) {
            //do stuff with exception
            iox.printStackTrace();
        }

    }


}
