import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.naming.NamingException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class NodePrinter {

    public static void main(String[] args) throws ClassCastException, MalformedURLException, RemoteException, NotBoundException, LoginException, NoSuchWorkspaceException, RepositoryException, NamingException {
        System.out.println("Welcome to the scripps cq reports interface.");
        //Connection
        //Repository repository = JcrUtils.getRepository("http://localhost:4502/crx/server");
        Repository repository = JcrUtils.getRepository("http://auth.scrippsnetworks.com/crx/server");
        //Workspace Login
        SimpleCredentials creds = new SimpleCredentials("scrippsreport", "scripps1".toCharArray());
        Session session = null;
        session = repository.login(creds, "crx.default");
        //List name of the current workspace
        System.out.println("Workspace: " + session.getWorkspace().getName() + "\n");

        //Create a node that represents the root node
        Node root = session.getRootNode();
        System.out.println("Root Node: " + root.getPath());
        //System.out.println(root.getProperty("sling:target").getString());

        // Retrieve content from root node
        printChildren(root.getNode("content/dam/images/cook/fullset"));
    }

    public static void printChildren(Node node) throws RepositoryException {

        String svalues = "";
        String imagename = "";
        String lvalues = "";
        boolean b;
        svalues = svalues + "IMAGE REPORT" + "\n";
        svalues = svalues + "cq:name,cq:lastReplicationAction,cq:path" + "\n";
        // Loop through each child(i.e Each Image Parent Node)
        for (NodeIterator nia = node.getNodes(); nia.hasNext(); ) {
            Node na = nia.nextNode();
            //System.out.println("Image Parent Node Name is "+ na.getName());
            imagename = na.getName().toString();
            b = imagename.endsWith("jpg");
            if (b) {

                System.out.println("Image Name is " + imagename);
                svalues = svalues + getSelectedFields(na, imagename, lvalues) + "\n";
                //System.out.println("Row value is "+ svalues);
            }


        }
        exportData(svalues);
    } //End of Printchildren method

    private static String getSelectedFields(Node node, String imagename, String lvalues) throws RepositoryException {

        String l_Field = "";
        int cnt = 0;
        String v_name = "";
        String v_lastReplicationAction = "";
        Boolean properityexists = true;
        //loop through underlying nodes
        //System.out.println("Input node for getSelectedFields is "+ node.getName());
        for (NodeIterator ni = node.getNodes(); ni.hasNext(); ) {
            Node n = ni.nextNode();
            System.out.println("Node name is " + n.getName());
            //Look into jcr:content node only
            if (n.getName().equals("jcr:content")) {
                // Look for the properties(fields)
                for (PropertyIterator pi = n.getProperties(); pi.hasNext(); ) {
                    //System.out.println("No of properites is "+ pi.getSize());
                    Property p = pi.nextProperty();
                    l_Field = p.getName();
                    //System.out.println("Property name is "+ l_Field);
                    cnt = cnt + 1;

                    //System.out.println("No of properites is "+ cnt);
                    switch (l_Field) {

                        case "cq:lastReplicationAction":
                            if (p.getDefinition().isMultiple()) {
                                Value[] values = p.getValues();
                                for (int i = 0; i < values.length; i++) {
                                    v_lastReplicationAction = values[i].getString();
                                }//End of values for loop
                            } else {
                                v_lastReplicationAction = p.getValue().getString();
                            }//End if isMultiple
                            break;

                        default:
                            break;
                    }//End switch statement
                }//End for property iterator
                //System.out.println("After properity iterator the name is "+ imagename+" and status is"+v_lastReplicationAction+" and path is "+n.getPath());
                lvalues = lvalues + imagename.replace(",", "") + "," + v_lastReplicationAction + "," + n.getPath();
            }//End if jcr:content validation

        }//End node iterator
        return lvalues;
    }//End getSelectedFields


    private static void exportData(String outtext) {


        try {
            String str = outtext;
            File newTextFile = new File("C:/files/Spool/Java/cq/Output/output.csv");

            FileWriter fw = new FileWriter(newTextFile);
            fw.append(str);
            fw.close();

        } catch (IOException iox) {
            //do stuff with exception
            iox.printStackTrace();
        }

    }


}
            
 
    
